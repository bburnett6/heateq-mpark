import numpy as np 
import matplotlib.pyplot as plt 
import gif
import sys
from math import floor, ceil

@gif.frame
def plot(t, u, name):
	plt.title(f'{name}\nt = {t}')
	plt.imshow(u, cmap='hot', origin='lower')
	plt.colorbar(orientation='horizontal')
	plt.clim(0.0, 1.0)

def init_u(n):
	u = np.zeros((n, n))

	for i in range(n-1):
		for j in range(n-1):
			u[i, j] = np.sin(i/(n-1) * np.pi) * np.sin(j/(n-1) * np.pi)
	
	return u 

def fi(n, u, t, dx, beta):
	fn = np.zeros((n, n))
	stencel = lambda x, p, q: -4.0*x[p, q] + x[p-1, q] + x[p+1, q] + x[p, q-1] + x[p, q+1]

	for i in range(1, n-1):
		for j in range(1, n-1):
			fn[i, j] = (beta/(dx*dx)) * stencel(u, i, j)

	return fn

def rk4_update_step(n, u, t, dt, dx, beta):
	k1 = fi(n, u, t, dx, beta)
	tmp = u + dt/2.0 * k1 
	k2 = fi(n, tmp, t+dt/2.0, dx, beta)
	tmp = u + dt/2.0 * k2 
	k3 = fi(n, tmp, t+dt/2.0, dx, beta)
	tmp = u + dt * k3
	k4 = fi(n, tmp, t+dt, dx, beta)
	unp1 = u + (1.0/6.0) * dt * (k1 + 2.0*k2 + 2.0*k3 + k4)
	return unp1

def rk4_driver(nx, xs, nt, h, dt, xi, xf, ti, tf, beta):
	u = init_u(nx)

	#frames = []
	#nframes = 50
	#frame_every = nt//nframes

	ts = np.linspace(ti, tf, nt, endpoint=False)
	dt = ts[1] - ts[0]
	t_f = ti 
	for i, t in enumerate(ts):
		t_f = t_f + dt 
		u = rk4_update_step(nx, u, t, dt, h, beta)
		#if i % frame_every == 0:
		#	frames.append(plot(t, xs, beta, u, nt))
	print(f'final time: {t_f}')
	#print(u)
	#gif.save(frames, f'heat-1d-{nt}-a.gif', duration=5, unit='s', between='startend')

	return u, t

def init_a(n, gamma):
	"""
	for dirichlet boundary conditions, the first and last 
	submatrix on the diagonals are I. This is why the loop
	goes from n to n*(n-1). The corners of the inner 
	submatrices are 1 because they represent the edges. The
	final matrix for n=4, gamma=2 looks something like
	[[ 1.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.]
	 [ 0.  1.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.]
	 [ 0.  0.  1.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.]
	 [ 0.  0.  0.  1.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.]
	 [ 0.  0.  0.  0.  1.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.]
	 [ 0.  2.  0.  0.  2. -8.  2.  0.  0.  2.  0.  0.  0.  0.  0.  0.]
	 [ 0.  0.  2.  0.  0.  2. -8.  2.  0.  0.  2.  0.  0.  0.  0.  0.]
	 [ 0.  0.  0.  0.  0.  0.  0.  1.  0.  0.  0.  0.  0.  0.  0.  0.]
	 [ 0.  0.  0.  0.  0.  0.  0.  0.  1.  0.  0.  0.  0.  0.  0.  0.]
	 [ 0.  0.  0.  0.  0.  2.  0.  0.  2. -8.  2.  0.  0.  2.  0.  0.]
	 [ 0.  0.  0.  0.  0.  0.  2.  0.  0.  2. -8.  2.  0.  0.  2.  0.]
	 [ 0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  1.  0.  0.  0.  0.]
	 [ 0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  1.  0.  0.  0.]
	 [ 0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  1.  0.  0.]
	 [ 0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  1.  0.]
	 [ 0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  1.]]
	"""
	a = np.eye(n*n)

	for i in range(n, n * (n-1)):
		if (i % n != 0) and (i % n != (n-1)):
			a[i, i+1] = gamma
			a[i, i] = -4.0 * gamma
			a[i, i-1] = gamma
			a[i, i-n] = gamma
			a[i, i+n] = gamma

	return a

def init_b(n, a, g):
	""" 
	b for liniear solve of y(1) = X + g*dt*F(y(1)) 
	Where X can be un or un + whatever.
	THIS IS BUGGY!!!
	Should only be a/2 for all values in a that are not 1. 
	Those values represent edges for dirichlet boundaries.
	Currently this works because the edges are all 0 and
	2*0 is still zero but be aware this is technically not
	correct
	"""
	b = np.eye(n*n) - g * a 

	return b

def sdirk_update_step(n, u, t, dt, dx, beta):
	gamma = beta*dt/(dx*dx)
	g = (np.sqrt(3.0) + 3.0) / 6.0
	a = init_a(n, gamma)
	b = init_b(n, a, g)
	#np.set_printoptions(linewidth=500)
	#print(a)
	#print(b)

	unp1 = u 
	#Stage 1
	u1 = np.linalg.solve(b, np.reshape(u, n*n))

	#Stage 2
	u2 = np.linalg.solve(b, np.reshape(u, n*n) + (1.0 - 2.0*g) * np.matmul(a, u1))

	#Update
	unp1 = u + np.reshape(np.matmul(a/2.0, u1), (n, n)) + np.reshape(np.matmul(a/2.0, u2), (n, n))

	return unp1

def sdirk_driver(nx, xs, nt, h, dt, xi, xf, ti, tf, beta):
	u = init_u(nx)

	frames = []
	nframes = 50
	frame_every = nt//nframes

	ts = np.linspace(ti, tf, nt, endpoint=False)
	dt = ts[1] - ts[0]
	t_f = ti 
	for i, t in enumerate(ts):
		t_f = t_f + dt 
		u = sdirk_update_step(nx, u, t, dt, h, beta)
	#	if i % frame_every == 0:
	#		frames.append(plot(t, u, f"sdirk {nt}"))
	print(f'final time: {t_f}')
	#print(u)
	#gif.save(frames, f'heat-1d-{nt}-sdirk.gif', duration=5, unit='s', between='startend')

	return u, t

def round_nearest_hundred(x):
	return int(ceil(x / 100.0)) * 100

def main():
	beta = 1.0
	ti = 0.0
	tf = 0.1
	xi = 0.0
	xf = 1.0

	nx = 20 #nx = ny => dx = dy = h
	x = np.linspace(xi, xf, nx)
	h = x[1] - x[0]
	print(f'Grid points: {nx}')

	min_r = 0.5 #r  = beta*dt/dx^2 => dt = r/beta*dx^2 => nt = beta/r/dx^2
	min_nt = round_nearest_hundred(beta / min_r / (h*h))

	ref_nt = 2**7 * min_nt
	#ref_dt = (tf - ti) / ref_nt
	ts = np.linspace(ti, tf, ref_nt, endpoint=False)
	ref_dt = ts[1] - ts[0]
	print(f'reference nt = {ref_nt}')
	print(f'reference dt = {ref_dt}')
	uf_ref, t_f = rk4_driver(nx, x, ref_nt, h, ref_dt, xi, xf, ti, tf, beta)
	#e_ex = exact(x, t_f, beta)
	#print(e_ex)
	#print(uf_ref)
	#print(uf_ref - e_ex)
	#print(f'error (abs) for reference (dt = {dt}) vs exact is {np.amax(np.abs(uf_ref - e_ex))}')
	#print(f'error (L2) for reference (dt = {dt}) vs exact is {np.sqrt(1./nx*np.sum(np.power(uf_ref - e_ex, 2)))}')
	#print()

	exponents = range(7) #goes from minimum nt to before ref_dt exponent nt
	m_ex_errs = []
	m_ref_errs = []
	l_ex_errs = []
	l_ref_errs = []
	e3_errs = []
	dts = []
	nts = list(map(lambda p: p*100, range(6, 14) ))

	#for e_n in exponents:
		#nt = 2**e_n * min_nt
	for nt in nts:
		#dt = (tf - ti) / nt
		ts = np.linspace(ti, tf, nt, endpoint=False)
		dt = ts[1] - ts[0]
		print(f'nt = {nt}')
		print(f'dt = {dt}')
		#nts.append(nt)
		dts.append(dt)
		uf, t_f = sdirk_driver(nx, x, nt, h, dt, xi, xf, ti, tf, beta)
		#print(uf)
		print("maximum error: amax(abs(u - e))")
		#m_e_ex = np.amax(np.abs(uf - e_ex))
		#m_ex_errs.append(m_e_ex)
		m_e_ref = np.amax(np.abs(uf - uf_ref))
		m_ref_errs.append(m_e_ref)
		#print(f'error for dt = {dt} vs exact is {m_e_ex}')
		print(f'error for dt = {dt} vs reference is {m_e_ref}')
		print("L2 error: sqrt(1/N*sum(|u - e|^2))")
		#l_e_ex = np.sqrt(dx*np.sum(np.power(uf - e_ex, 2)))
		#l_ex_errs.append(l_e_ex)
		l_e_ref = np.sqrt(h*np.sum(np.power(uf - uf_ref, 2)))
		l_ref_errs.append(l_e_ref)
		#print(f'error for dt = {dt} vs exact is {l_e_ex}')
		print(f'error for dt = {dt} vs reference is {l_e_ref}')
		print()

	orders = []
	for p in range(1, len(dts)):
		orders.append(np.log(m_ref_errs[p]/m_ref_errs[p-1])/np.log(dts[p]/dts[p-1]))

	print(f'orders: {orders}')
	print(f'average: {sum(orders)/len(orders)}')
	print(f'errors: {m_ref_errs}')
	print(f'nts: {nts}')
	print(f'dts: {dts}')


if __name__ == '__main__':
	main()