import numpy as np 
import sys

def init_a(n, gamma):
	a = np.eye(n*n)

	for i in range(n, n * (n-1)):
		if (i % n != 0) and (i % n != (n-1)):
			a[i, i+1] = gamma
			a[i, i] = -4.0 * gamma
			a[i, i-1] = gamma
			a[i, i-n] = gamma
			a[i, i+n] = gamma

	return a

if __name__ == '__main__':
	n = int(sys.argv[1])
	a = init_a(n, 2)
	np.set_printoptions(linewidth=500)
	print(a)
	nonzeros = np.any(a-np.eye(n*n), axis=1)
	x = 0
	for v in nonzeros:
		if v:
			x = x + 1
	print(x)