!gfortran -o construct_a.x construct_a.f08 -cpp -DFULL_TYPE=real64

#ifndef NX
#define NX 20
#endif
#ifndef DEBUG
#define DEBUG 0
#endif
#ifndef FULL_TYPE
#define FULL_TYPE real128
#endif
#ifndef REDU_TYPE
#define REDU_TYPE real128
#endif

module utility_m
contains

	subroutine linspace(from, to, array, endpoint)
	!thank you: https://stackoverflow.com/a/57211848
	!modified to add endpoint true/false support
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
    real(fp), intent(in) :: from, to
    real(fp), intent(out) :: array(:)
    logical, intent(in) :: endpoint
    real(fp) :: range
    integer :: n, d, i

    n = size(array)
    if (endpoint) then 
    	d = -1
    else
    	d = 0
    end if 

    range = to - from

    if (n == 0) return

    if (n == 1) then
        array(1) = from
        return
    end if

    do i=1, n
        array(i) = from + range * (i - 1) / (n + d)
    end do
	end subroutine linspace

	subroutine eye_fp(n, matrix)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	integer, value, intent(in) :: n 
	real(fp), intent(out) :: matrix(n, n)
	integer :: i

	matrix = 0.0
	do i=1,n 
		matrix(i, i) = 1.0_fp
	end do 

	end subroutine eye_fp

	subroutine eye_rp(n, matrix)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	integer, value, intent(in) :: n 
	real(rp), intent(out) :: matrix(n, n)
	integer :: i

	matrix = 0.0
	do i=1,n 
		matrix(i, i) = 1.0_rp
	end do 

	end subroutine eye_rp

	subroutine printmatrix(b,n,m)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	!http://math.hawaii.edu/~gautier/math_190_lecture_11.pdf
	integer::n,m
	real(fp)::b(n,m) !n = # rows, m = # columns
	do i=1,n; print '(30f6.2)',b(i,1:m); enddo
	end subroutine printmatrix

	subroutine printmatrix_rp(b,n,m)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	!http://math.hawaii.edu/~gautier/math_190_lecture_11.pdf
	integer::n,m
	real(rp)::b(n,m) !n = # rows, m = # columns
	do i=1,n; print '(20f6.2)',b(i,1:m); enddo !printing for half precision not supported so comment out
	end subroutine printmatrix_rp

end module utility_m

program main
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	use utility_m 
	implicit none 
	integer :: n, i 
	real(fp) :: gamma = 2.0
	real(fp), allocatable :: a(:, :)
	character(len=32) :: arg

	call getarg(1, arg)
	read(arg, *)n

	allocate(a(n*n, n*n))
	call eye_fp(n*n, a)

	do i=n+1,n*(n-1)
		print *, 'i=', i, ' n=', n, 'mod(i,n)=', mod(i, n)
		if ((mod(i, n) /= 0) .and. (mod(i, n) /= 1)) then 
			a(i, i+1) = gamma
			a(i, i) = -4.0 * gamma 
			a(i, i-1) = gamma 
			a(i, i-n) = gamma 
			a(i, i+n) = gamma
		end if
	end do 

	call printmatrix(a, n*n, n*n)

	deallocate(a)

end program main