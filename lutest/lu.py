import numpy as np 

def lu_decomp(n, A):
	P = []
	for i in range(n):
		P.append(i) 

	for i in range(n):
		maxA = 0.0
		imax = i 

		for k in range(i, n):
			absA = np.abs(A[k, i])
			if absA > maxA:
				maxA = absA
				imax = k 

		if maxA < 0.00000000001:
			print("AHHHHHHHH")

		if imax != i:
			j = P[i]
			P[i] = P[imax]
			P[imax] = j 

			#tmp = A[i, :]
			#A[i, :] = A[imax, :]
			#A[imax, :] = tmp 
			A[[i, imax]] = A[[imax, i]]

			#P[n]++

		for j in range(i+1, n):
			A[j, i] /= A[i, i]
			for k in range(i+1, n):
				A[j, k] -= A[j, i] * A[i, k]

	return A, P 

def lu_solve(n, A, P, b):
	#x = np.array(b, copy=True)
	x = np.zeros(n)
	for i in range(n):
		x[i] = b[P[i]]

		for k in range(0, i):
			x[i] -= A[i, k] * x[k]
	#print(f'xtmp: {x}')

	for i in range(n-1, i, -1):
		for k in range(i+1, n):
			x[i] -= A[i, k] * x[k]

		x[i] /= A[i, i]

	return x 

def main():
	a = np.array([[1, 4, 4], [2, 4, 6], [2, 2, 4]], dtype=np.float64)
	b = np.array([1., 1., 2.])
	a_save = a 
	print(f'A:\n {a}')
	LU, P = lu_decomp(3, a)
	x = lu_solve(3, LU, P, b)
	print(f'LU:\n {LU}')
	print(f'P:\n {P}')
	print(f'b:\n {b}')
	print(f'x:\n {x}')
	print(f'Ax:\n {np.matmul(a_save, x)}')

if __name__ == '__main__':
	main()