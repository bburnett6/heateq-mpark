//gcc -o lu.x lu.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* INPUT: A - array of pointers to rows of a square matrix having dimension N
 *        Tol - small tolerance number to detect failure when the matrix is near degenerate
 * OUTPUT: Matrix A is changed, it contains a copy of both matrices L-E and U as A=(L-E)+U such that P*A=L*U.
 *        The permutation matrix is not stored as a matrix, but in an integer vector P of size N+1 
 *        containing column indexes where the permutation matrix has "1". The last element P[N]=S+N, 
 *        where S is the number of row exchanges needed for determinant computation, det(P)=(-1)^S    
 */
int LUPDecompose(double **A, int N, double Tol, int *P) {

    int i, j, k, imax; 
    double maxA, *ptr, absA;

    for (i = 0; i <= N; i++)
        P[i] = i; //Unit permutation matrix, P[N] initialized with N

    for (i = 0; i < N; i++) {
        maxA = 0.0;
        imax = i;

        for (k = i; k < N; k++)
            if ((absA = fabs(A[k][i])) > maxA) { 
                maxA = absA;
                imax = k;
            }

        if (maxA < Tol) return 0; //failure, matrix is degenerate

        if (imax != i) {
            //pivoting P
            j = P[i];
            P[i] = P[imax];
            P[imax] = j;

            //pivoting rows of A
            ptr = A[i];
            A[i] = A[imax];
            A[imax] = ptr;

            //counting pivots starting from N (for determinant)
            P[N]++;
        }

        for (j = i + 1; j < N; j++) {
            A[j][i] /= A[i][i];

            for (k = i + 1; k < N; k++)
                A[j][k] -= A[j][i] * A[i][k];
        }
    }

    return 1;  //decomposition done 
}

/* INPUT: A,P filled in LUPDecompose; b - rhs vector; N - dimension
 * OUTPUT: x - solution vector of A*x=b
 */
void LUPSolve(double **A, int *P, double *b, int N, double *x) {

    for (int i = 0; i < N; i++) {
        x[i] = b[P[i]];

        for (int k = 0; k < i; k++)
            x[i] -= A[i][k] * x[k];
        
    }

    for (int i = N - 1; i >= 0; i--) {
        for (int k = i + 1; k < N; k++)
            x[i] -= A[i][k] * x[k];

        x[i] /= A[i][i];
    }
}

void mallocDoubleArr(double ***arr, int size)
{
    int i, j;

    *arr = malloc(size * sizeof(double*));

    for(i = 0; i < size; i++)
    {
        (*arr)[i]= malloc(size*sizeof(double));
    }
}

int main()
{
	double a[3][3] = {
		{1.0, 4.0, 4.0},
		{2.0, 4.0, 6.0},
		{2.0, 2.0, 4.0}
	};
	double** LU;
	mallocDoubleArr(&LU, 3);
	double b[3] = {1.0, 1.0, 2.0};
	double x[3];
	int p[4];

	printf("A:\n");
	for (int i=0; i < 3; i++){
		for (int j=0; j < 3; j++){
			printf("%f ", a[i][j]);
			LU[i][j] = a[i][j];
		}
		printf("\n");
	}
	printf("\n");

	printf("b:\n");
	for (int i=0; i < 3; i++)
		printf("%f ", b[i]);
	printf("\n\n");

	int err = LUPDecompose(LU, 3, 0.0000001, p);
	if (err != 1)
		printf("AHHHHHHHHH\n");
	printf("LU:\n");
	for (int i=0; i < 3; i++){
		for (int j=0; j < 3; j++){
			printf("%f ", LU[i][j]);
		}
		printf("\n");
	}
	printf("\n");

	LUPSolve(LU, p, b, 3, x);
	printf("x:\n");
	for (int i=0; i < 3; i++)
		printf("%f ", x[i]);
	printf("\n\n");


	return 0;
}