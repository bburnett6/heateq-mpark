!gfortran -o lu.x lu.f08 -cpp -DFULL_TYPE=real64 -DREDU_TYPE=real32

#ifndef NX
#define NX 20
#endif
#ifndef DEBUG
#define DEBUG 0
#endif
#ifndef FULL_TYPE
#define FULL_TYPE real128
#endif
#ifndef REDU_TYPE
#define REDU_TYPE real128
#endif

module utility_m
contains

	subroutine linspace(from, to, array, endpoint)
	!thank you: https://stackoverflow.com/a/57211848
	!modified to add endpoint true/false support
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
    real(fp), intent(in) :: from, to
    real(fp), intent(out) :: array(:)
    logical, intent(in) :: endpoint
    real(fp) :: range
    integer :: n, d, i

    n = size(array)
    if (endpoint) then 
    	d = -1
    else
    	d = 0
    end if 

    range = to - from

    if (n == 0) return

    if (n == 1) then
        array(1) = from
        return
    end if

    do i=1, n
        array(i) = from + range * (i - 1) / (n + d)
    end do
	end subroutine linspace

	subroutine eye_fp(n, matrix)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	integer, value, intent(in) :: n 
	real(fp), intent(out) :: matrix(n, n)
	integer :: i

	matrix = 0.0
	do i=1,n 
		matrix(i, i) = 1.0_fp
	end do 

	end subroutine eye_fp

	subroutine eye_rp(n, matrix)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	integer, value, intent(in) :: n 
	real(rp), intent(out) :: matrix(n, n)
	integer :: i

	matrix = 0.0
	do i=1,n 
		matrix(i, i) = 1.0_rp
	end do 

	end subroutine eye_rp

	subroutine printmatrix(b,n,m)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	!http://math.hawaii.edu/~gautier/math_190_lecture_11.pdf
	integer::n,m
	real(fp)::b(n,m) !n = # rows, m = # columns
	do i=1,n; print '(20f6.2)',b(i,1:m); enddo
	end subroutine printmatrix

	subroutine printmatrix_rp(b,n,m)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	!http://math.hawaii.edu/~gautier/math_190_lecture_11.pdf
	integer::n,m
	real(rp)::b(n,m) !n = # rows, m = # columns
	do i=1,n; print '(20f6.2)',b(i,1:m); enddo !printing for half precision not supported so comment out
	end subroutine printmatrix_rp

end module utility_m

module mylu_m
contains
!implemented based on the wikipedia article on LU decomp
!Translated version of https://en.wikipedia.org/wiki/LU_decomposition#C_code_example
	subroutine lu_solve(n, Ain, b, x)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	use utility_m
	integer, value, intent(in) :: n 
	real(rp), intent(in) :: Ain(n, n), b(n) !Ain is read only
	real(rp), intent(out) :: x(n)
	real(rp) :: A(n, n)
	integer :: P(n+1), i, k

	A = Ain
	!Decomp
	call lu_decomp(n, A, P)

	!call printmatrix_rp(A, n, n)
	!print *, "P: ", P(:)
	
	!fwd solve
	do i=1,n 
		x(i) = b(P(i))

		do k=1,i-1
			x(i) = x(i) - A(i, k) * x(k)
		end do 
	end do

	!bwd solve
	do i=n,1,-1
		do k=i+1,n 
			x(i) = x(i) - A(i, k) * x(k)
		end do 
		x(i) = x(i) / A(i, i)
	end do 

	end subroutine lu_solve

	subroutine lu_decomp(n, A, P)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	integer, value, intent(in) :: n
	real(rp), intent(inout) :: A(n, n) 
	integer, intent(out) :: P(n+1)
	integer :: i, j, k, imax 
	real(rp) :: maxA, absA, tmp(n), tol 

	tol = epsilon(tol)

	!initialize permutation
	do i=1,n 
		P(i) = i 
	end do

	!do inplace decomp
	do i=1,n 
		maxA = 0.0_rp 
		imax = i 

		do k=i,n 
			absA = abs(A(k, i))
			if (absA > maxA) then 
				maxA = absA
				imax = k 
			end if
		end do 

		if (maxA < tol) then 
			print *, "DEGENERATE MATRIX! OH NO!"
		end if 

		if (imax .ne. i) then 
			!pivoting P
			j = P(i)
			P(i) = P(imax)
			P(imax) = j 

			!pivoting rows of A 
			tmp(:) = A(i, :)
			A(i, :) = A(imax, :)
			a(imax, :) = tmp(:)

			P(n+1) = P(n+1) + 1 
		end if 

		do j=i+1,n 
			A(j, i) = A(j, i) / A(i, i)

			do k=i+1,n 
				A(j, k) = A(j, k) - A(j, i) * A(i, k)
			end do 
		end do  
	end do 

	end subroutine lu_decomp

end module mylu_m

program main
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	use utility_m
	use mylu_m 
	implicit none 
	real(rp) :: A(3, 3), b(3), x(3)
	
	A = reshape((/1.0_rp, 2.0_rp, 2.0_rp, 4.0_rp, 4.0_rp, 2.0_rp, 4.0_rp, 6.0_rp, 4.0_rp/), shape(A))
	print *, 'A:'
	call printmatrix_rp(A, 3, 3)
	b = reshape((/1.0_rp, 1.0_rp, 2.0_rp/), shape(b))
	print *, "b:"
	call printmatrix_rp(b, 3, 1)
	print *, "lu:"
	call lu_solve(3, A, b, x)
	print *, "x:"
	call printmatrix_rp(x, 3, 1)
	print *, "Ax:"
	call printmatrix_rp(matmul(A, x), 3, 1)


end program main