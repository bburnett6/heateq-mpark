# Heat Equation MPARK

This repository houses the work done to solve the heat equation using mixed precision additive Runge-Kutta time stepping methods.

## 1D Heat

The confirmation code for this was done in python and can be found in `./confirm-1dheat`. Running the `rk4.py` and `impmid.py` files will show the data used to show proper convergence order to a solution using the RK4 at a small time step. 

The `./1dheat` directory then holds the Fortran implementation that makes use of the mixed precision techniques central to this study. Running the `verify_method.py` with a directory name of a method as an argument will run a quick convergence test (example: `python verify_method.py impmid` or `python verify_method.py impmid-1p`). The `error_experiment.py` file will run the benchmarking experiment on all methods outputting json files in the respective method directories. The json files are used with the `plot_results.py` file (on an Intel platform use the `plot_results_x86.py`) by passing the method name matching the directory as an argument (example: `python plot_results_x86.py impmid`). The `arm_energy_experiment.py` uses the `postproc_perf_energy.py` file to run an energy benchmark on an aarch64 platform (the A64FX was the target of this project). The energy benchmark does a comprehensive benchmark and should be the only experiment run on the Arm system. The `error_experiment.py` is there to test and verify on my Intel system that the initial work was done on. The `run_experiment.sh` file is the sbatch script used on Ookami to run the energy benchmark. This batch script relies on a python venv that has numpy, pandas, and matplotlib installed.

## 2D Heat

The 2D heat shares an identical structure to the 1D Heat but is housed in `./confirm-2dheat` and `./2dheat`. Inside the `./confirm-2dheat` are files used to test and verify that the structure for the matricies used in the solvers is correct (both for python and fortran) called `construct_a.*` and can be run by passing the size of one dimension of the problem to be solved (example: `./construct_a.x 4`). 

## LU Decomposition

For this project, an in-house linear solver was created using an LU Solver based on the sample codes from [Wikipedia](https://en.wikipedia.org/wiki/LU_decomposition#Code_examples). This was done because linear solvers like LAPACK only cover double and single precision and we are targeting the use of half and quadruple precision. One of the goals of this project is to show that solvers in all precisions could be beneficial. The tests for this can be found in `./lutest`. The completed Fortran module is then used as the solver in all of the codes in this project

## Environment used

The target for this project was the Arm A64FX processor available in the Ookami cluster at [Stony Brook University](https://www.stonybrook.edu/ookami/). Below is a sample workflow for using this repositiory on that system:

```
git clone https://gitlab.com/bburnett6/heateq-mpark.git
cd heateq-mpark
module load python/3.9.5
python -m venv mpark-env 
pip install numpy pandas matplotlib #might need to be on one of the fj-debug nodes
cd 1dheat
sbatch run_experiment.sh
```
