import numpy as np 
import matplotlib.pyplot as plt 
import gif
import sys
from math import ceil, floor

@gif.frame
def plot(t, xs, beta, u, name):
	plt.title(f'{name}\nt = {t}')
	plt.xlim([xs[0], xs[-1]])
	plt.ylim([-1.0, 1.0])
	plt.plot(xs, u, 'r.', label='rk4')
	plt.plot(xs, exact(xs, t, beta), 'b-', label='exact')
	plt.legend()

def exact(x, t, beta):
	v = np.exp(-4.0*beta*t*np.pi**2) * np.sin(2.0*np.pi*x)
	v[-1] = 0.0
	return v

def init_u(xs):
	u = np.sin(2.0*np.pi*xs)
	u[-1] = 0
	return u 

def fi(n, u, t, dx, beta):
	fn = np.zeros(n)

	for i in range(1, n-1):
		fn[i] = beta/(dx*dx) * (u[i-1] + u[i+1] - 2.0*u[i])
	fn[-1] = 0
	return fn

def rk4_update_step(n, u, t, dt, dx, beta):
	k1 = fi(n, u, t, dx, beta)
	tmp = u + dt/2.0 * k1 
	k2 = fi(n, tmp, t+dt/2.0, dx, beta)
	tmp = u + dt/2.0 * k2 
	k3 = fi(n, tmp, t+dt/2.0, dx, beta)
	tmp = u + dt * k3
	k4 = fi(n, tmp, t+dt, dx, beta)
	unp1 = u + (1.0/6.0) * dt * (k1 + 2.0*k2 + 2.0*k3 + k4)
	return unp1

def rk4_driver(nx, xs, nt, dx, dt, xi, xf, ti, tf, beta):
	u = init_u(xs) 
	#frames = []
	#nframes = 50
	#frame_every = nt//nframes 
	t = 0.0
	dt_tmp = dt 
	np.set_printoptions(linewidth=500)
	for i in range(nt):
		u = rk4_update_step(nx, u, t, dt_tmp, dx, beta)
		t = t + dt_tmp 
	#	if i % frame_every == 0:
	#		frames.append(plot(t, xs, beta, u, nt))

	if t < tf:
		print(f'understep time: {t}')
		print(f'understep dt: {tf - t}')
		print(f'understep time after correction: {t + (tf - t)}')
		dt_tmp = tf - t 
		u = update_step(nx, u, t, dt_tmp, dx, beta)
		t = t + dt_tmp
	print(f'final time: {t}')

	#print(u)
	#gif.save(frames, f'heat-1d-{nt}-a.gif', duration=5, unit='s', between='startend')

	return u, t 

def rk4_driver_linspace(nx, xs, nt, dx, xi, xf, ti, tf, beta):
	u = init_u(xs)

	ts = np.linspace(ti, tf, nt, endpoint=False)
	dt = ts[1] - ts[0]
	t_f = ti 
	for i, t in enumerate(ts):
		t_f = t_f + dt 
		u = rk4_update_step(nx, u, t, dt, dx, beta)
		#if i % frame_every == 0:
		#	frames.append(plot(t, xs, beta, u, nt))
	print(f'final time: {t_f}')
	#print(u)
	#gif.save(frames, f'heat-1d-{nt}-a.gif', duration=5, unit='s', between='startend')

	return u, t

def init_a(nx, gamma):
	a = np.zeros((nx, nx))

	for i in range(1, nx-1):
		a[i, i+1] = gamma
		a[i, i] = -2.0 * gamma
		a[i, i-1] = gamma
	a[0, 0] = 1.0
	a[nx-1, nx-1] = 1.0

	return a 

def init_b(nx, a):
	b = np.eye(nx) - a / 2.0

	return b


def imp_update_step(nx, u, t, dt, dx, beta):
	gamma = beta*dt/(dx*dx)
	a = init_a(nx, gamma)
	b = init_b(nx, a)
	unp1 = u
	#if (t == 0.0):
	#	np.set_printoptions(linewidth=500)
	#	print(a)
	#	print(b)
	u1 = np.linalg.solve(b, u)
	unp1 = u + np.matmul(a, u1)
	#if (t == 0.0):
	#	np.set_printoptions(linewidth=500)
	#	print(unp1)

	return unp1

def impmid_driver(nx, xs, nt, dx, dt, xi, xf, ti, tf, beta):
	u = init_u(xs) 
	
	#frames = []
	#nframes = 50
	#frame_every = nt//nframes 
	t = 0.0
	np.set_printoptions(linewidth=500)
	for i in range(nt):
		if t + dt > tf:
			dt = tf - t  
		u = imp_update_step(nx, u, t, dt, dx, beta)
		t = t + dt
		#if i % frame_every == 0:
		#	frames.append(plot(t, xs, beta, u, nt))

	if t < tf:
		dt_tmp = tf - t 
		u = imp_update_step(nx, u, t, dt_tmp, dx, beta)
		t = t + dt_tmp
	print(t)
	#print(u)
	#gif.save(frames, f'heat-1d-{nt}-impmid.gif', duration=5, unit='s', between='startend')

	return u, t

def impmid_driver_linspace(nx, xs, nt, dx, xi, xf, ti, tf, beta):
	u = init_u(xs)

	frames = []
	nframes = 50
	frame_every = nt//nframes
	ts = np.linspace(ti, tf, nt, endpoint=False)
	dt = ts[1] - ts[0]
	t_f = ti 
	for i, t in enumerate(ts):
		t_f = t_f + dt 
		u = imp_update_step(nx, u, t, dt, dx, beta)
	#	if i % frame_every == 0:
	#		frames.append(plot(t, xs, beta, u, nt))
	print(f'final time: {t_f}')
	#print(u)
	#gif.save(frames, f'heat-1d-{nt}-a.gif', duration=5, unit='s', between='startend')

	return u, t

def round_nearest_hundred(x):
	return int(ceil(x / 100.0)) * 100

def main():
	beta = 1.0
	ti = 0.0
	tf = 0.1
	xi = 0.0
	xf = 1.0

	nx = 50
	x = np.linspace(xi, xf, nx)
	dx = x[1] - x[0]
	print(f'Grid points: {nx}')

	min_r = 0.5 #r  = beta*dt/dx^2 => dt = r/beta*dx^2 => nt = beta/r/dx^2
	min_nt = round_nearest_hundred(beta / min_r / (dx*dx))

	ref_nt = 2**9 * min_nt
	#ref_dt = (tf - ti) / ref_nt
	ts = np.linspace(ti, tf, ref_nt, endpoint=False)
	ref_dt = ts[1] - ts[0]
	print(f'reference nt = {ref_nt}')
	print(f'reference dt = {ref_dt}')
	#uf_ref, t_f = driver(nx, x, ref_nt, dx, ref_dt, xi, xf, ti, tf, beta)
	uf_ref, t_f = rk4_driver_linspace(nx, x, ref_nt, dx, xi, xf, ti, tf, beta)
	e_ex = exact(x, t_f, beta)
	#print(e_ex)
	#print(uf_ref)
	#print(uf_ref - e_ex)
	print(f'error (abs) for reference (dt = {ref_dt}) vs exact is {np.amax(np.abs(uf_ref - e_ex))}')
	print(f'error (L2) for reference (dt = {ref_dt}) vs exact is {np.sqrt(1./nx*np.sum(np.power(uf_ref - e_ex, 2)))}')
	print()

	exponents = range(7) #goes from minimum nt to before ref_dt exponent nt
	m_ex_errs = []
	m_ref_errs = []
	l_ex_errs = []
	l_ref_errs = []
	e3_errs = []
	dts = []
	nts = list(map(lambda p: p*100, range(6, 14) ))

	#for e_n in exponents:
		#nt = 2**e_n * min_nt
	for nt in nts:
		#dt = (tf - ti) / nt
		ts = np.linspace(ti, tf, nt, endpoint=False)
		dt = ts[1] - ts[0]
		print(f'nt = {nt}')
		print(f'dt = {dt}')
		#nts.append(nt)
		dts.append(dt)
		#uf, t_f = driver(nx, x, nt, dx, dt, xi, xf, ti, tf, beta)
		uf, t_f = impmid_driver_linspace(nx, x, nt, dx, xi, xf, ti, tf, beta)
		#print(uf)
		print("maximum error: amax(abs(u - e))")
		m_e_ex = np.amax(np.abs(uf - e_ex))
		m_ex_errs.append(m_e_ex)
		m_e_ref = np.amax(np.abs(uf - uf_ref))
		m_ref_errs.append(m_e_ref)
		print(f'error vs exact is {m_e_ex}')
		print(f'error vs reference is {m_e_ref}')
		print("L2 error: sqrt(1/N*sum(|u - e|^2))")
		l_e_ex = np.sqrt(dx*np.sum(np.power(uf - e_ex, 2)))
		l_ex_errs.append(l_e_ex)
		l_e_ref = np.sqrt(dx*np.sum(np.power(uf - uf_ref, 2)))
		l_ref_errs.append(l_e_ref)
		print(f'error vs exact is {l_e_ex}')
		print(f'error vs reference is {l_e_ref}')
		e3_e = abs(uf_ref[3] - uf[3])
		e3_errs.append(e3_e)
		print(f'error for for 3rd element is {e3_e}')
		print()

	orders = []
	for p in range(1, len(dts)):
		orders.append(np.log(m_ref_errs[p]/m_ref_errs[p-1])/np.log(dts[p]/dts[p-1]))
		#o = (np.log(m_ref_errs[p-1]) - np.log(m_ref_errs[p])) / np.log(2.0)
		#orders.append(o)
		#orders.append(np.log(e3_errs[p]/e3_errs[p-1])/np.log(rs[p]/rs[p-1]))

	print(f'orders: {orders}')
	print(f'average: {sum(orders)/len(orders)}')
	print(f'errors: {m_ref_errs}')
	print(f'nts: {nts}')
	print(f'dts: {dts}')


if __name__ == '__main__':
	main()