import subprocess as sp 
import sys
import os
import numpy as np 
#import matplotlib.pyplot as plt 
import json
import itertools
import multiprocessing as mp 

"""
Compile using included makefile
d_redu - The reduced precision data type {real32, real64, real128}
d_full - The full precision data type {same ^}

no return

Note: I did try using subprocess but make wouldn't use the variables.
so I just used os.system
"""
def compile(d_full, d_redu, nx):
	cmd = f"make fp={d_full} rp={d_redu} debug=0 nx={nx}"
	#print(cmd)
	#print(cmd.split())
	#sp.run(['make', f"full_prec='{d_full}'", f"redu_prec='{d_redu}'"], shell=True, check=True)
	#sp.run(cmd.split(), shell=True, check=True)
	stat = os.system(cmd)
	if stat != 0:
		print(f"ERROR: exit status {stat}")

"""
Run the program
nt - Program argument for nt, ie the number of timesteps

program outputs the time to run the method, so capture it and return
"""
def run_prog(nt, test_name, n_runs=5):
	#dt computation. This will be the dt in the run
	ti = 0.0
	tf = 0.1
	timesteps = np.linspace(ti, tf, nt, endpoint=False)
	dt = timesteps[1] - timesteps[0]
	#on to the runs
	t = 0
	e = 0
	for i in range(n_runs):
		out = sp.run([f'./{test_name}.x', f'{nt}'], capture_output=True)
		#print(out.stdout.split())
		lines = out.stdout.split()
		t = t + float(lines[2])
		e = e + float(lines[7])
	return t/n_runs, e/n_runs, dt 

"""
Run the full experiment for one of the tests
test_name - the name of the test to run. Name must match the directory 
and teh name of the test.

Writes the results of the experiment inside the test directory to a 
json file
"""
def experiment(test_name):
	workdir = os.path.join('./', test_name)
	if not os.path.exists(workdir):
		print(f"experiment directory {test_name} doesn't exist. Returning...")
	os.chdir(workdir)

	d_types = ['real128', 'real64', 'real32']
	nx = 50
	combos = itertools.combinations_with_replacement(d_types, 2)
	nts = list(map(lambda p: 10**p, range(2, 8)))
	
	runs = []
	for d_full, d_redu in combos:
		compile(d_full, d_redu, nx)
		run = {'redu': d_redu, 'full': d_full, 'errs': [], 'ts': [], 'dts': []}
		for nt in nts:
			print(f"Running {test_name} for nt={nt}")
			t, e, dt = run_prog(nt, test_name)
			run['dts'].append(dt)
			run['errs'].append(e)
			run['ts'].append(t)
		runs.append(run)

	with open(f'runs_x86_{test_name}.json', 'w') as f:
		json.dump(runs, f)

def main():
	#run the reference solution
	nx = sys.argv[1]
	os.chdir('./ref_sol')
	compile('real128', 'real128', nx)
	ref_nt = 10**8
	print(f"Running reference for {ref_nt}")
	out = sp.run([f'./rk4.x', f'{ref_nt}', '1'], capture_output=True)
	os.chdir('./..')
	#Use multiprocess to run all tests at once. 
	#Theoretically this shouldn't impact timing results because the 
	#tests are all serial. Requires >=4 hardware threads for this theory
	#to work
	tests = ['impmid', 'impmid-1p', 'impmid-2p']
	with mp.Pool(processes=len(tests)) as pool:
		pool.map(experiment, tests)

if __name__ == '__main__':
	main()