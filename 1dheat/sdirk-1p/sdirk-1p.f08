!gfortran -o sdirk.x sdirk.f08 -cpp -DFULL_TYPE=real64 -DREDU_TYPE=real32

#ifndef NX
#define NX 20
#endif
#ifndef DEBUG
#define DEBUG 0
#endif
#ifndef FULL_TYPE
#define FULL_TYPE real128
#endif
#ifndef REDU_TYPE
#define REDU_TYPE real128
#endif

module utility_m
contains

	subroutine linspace(from, to, array, endpoint)
	!thank you: https://stackoverflow.com/a/57211848
	!modified to add endpoint true/false support
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
    real(fp), intent(in) :: from, to
    real(fp), intent(out) :: array(:)
    logical, intent(in) :: endpoint
    real(fp) :: range
    integer :: n, d, i

    n = size(array)
    if (endpoint) then 
    	d = -1
    else
    	d = 0
    end if 

    range = to - from

    if (n == 0) return

    if (n == 1) then
        array(1) = from
        return
    end if

    do i=1, n
        array(i) = from + range * (i - 1) / (n + d)
    end do
	end subroutine linspace

	subroutine eye_fp(n, matrix)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	integer, value, intent(in) :: n 
	real(fp), intent(out) :: matrix(n, n)
	integer :: i

	matrix = 0.0
	do i=1,n 
		matrix(i, i) = 1.0_fp
	end do 

	end subroutine eye_fp

	subroutine eye_rp(n, matrix)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	integer, value, intent(in) :: n 
	real(rp), intent(out) :: matrix(n, n)
	integer :: i

	matrix = 0.0
	do i=1,n 
		matrix(i, i) = 1.0_rp
	end do 

	end subroutine eye_rp

	subroutine printmatrix(b,n,m)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	!http://math.hawaii.edu/~gautier/math_190_lecture_11.pdf
	integer::n,m
	real(fp)::b(n,m) !n = # rows, m = # columns
	do i=1,n; print '(20f6.2)',b(i,1:m); enddo
	end subroutine printmatrix

	subroutine printmatrix_rp(b,n,m)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	!http://math.hawaii.edu/~gautier/math_190_lecture_11.pdf
	integer::n,m
	real(rp)::b(n,m) !n = # rows, m = # columns
	!do i=1,n; print '(20f6.3)',b(i,1:m); enddo !printing for half precision not supported so comment out
	end subroutine printmatrix_rp

end module utility_m

module mylu_m
contains
!implemented based on the wikipedia article on LU decomp
!Translated version of https://en.wikipedia.org/wiki/LU_decomposition#C_code_example
!the module is split into full precision parts and reduced precision parts
!Each part has a full solver, a decompose, and a fwd/bwd only
	subroutine lu_solve_fp(n, Ain, b, x)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	use utility_m
	integer, value, intent(in) :: n 
	real(fp), intent(in) :: Ain(n, n), b(n) !Ain is read only
	real(fp), intent(out) :: x(n)
	real(fp) :: A(n, n)
	integer :: P(n+1), i, k

	A = Ain
	!Decomp
	call lu_decomp_fp(n, A, P)

	!call printmatrix_rp(A, n, n)
	!print *, "P: ", P(:)
	
	!fwd solve
	do i=1,n 
		x(i) = b(P(i))

		do k=1,i-1
			x(i) = x(i) - A(i, k) * x(k)
		end do 
	end do

	!bwd solve
	do i=n,1,-1
		do k=i+1,n 
			x(i) = x(i) - A(i, k) * x(k)
		end do 
		x(i) = x(i) / A(i, i)
	end do 

	end subroutine lu_solve_fp

	subroutine lu_decomp_fp(n, A, P)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	integer, value, intent(in) :: n
	real(fp), intent(inout) :: A(n, n) 
	integer, intent(out) :: P(n+1)
	integer :: i, j, k, imax 
	real(fp) :: maxA, absA, tmp(n), tol 

	tol = epsilon(tol)

	!initialize permutation
	do i=1,n 
		P(i) = i 
	end do

	!do inplace decomp
	do i=1,n 
		maxA = 0.0_rp 
		imax = i 

		do k=i,n 
			absA = abs(A(k, i))
			if (absA > maxA) then 
				maxA = absA
				imax = k 
			end if
		end do 

		if (maxA < tol) then 
			print *, "DEGENERATE MATRIX! OH NO!"
		end if 

		if (imax .ne. i) then 
			!pivoting P
			j = P(i)
			P(i) = P(imax)
			P(imax) = j 

			!pivoting rows of A 
			tmp(:) = A(i, :)
			A(i, :) = A(imax, :)
			a(imax, :) = tmp(:)

			P(n+1) = P(n+1) + 1 
		end if 

		do j=i+1,n 
			A(j, i) = A(j, i) / A(i, i)

			do k=i+1,n 
				A(j, k) = A(j, k) - A(j, i) * A(i, k)
			end do 
		end do  
	end do 

	end subroutine lu_decomp_fp

	subroutine lu_fwdbwd_fp(n, A, b, P, x)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	integer, value, intent(in) :: n 
	real(fp), intent(in) :: A(n, n), b(n) !A is a pre-decomposed matrix from lu_decomp_xx
	real(fp), intent(out) :: x(n)
	integer, intent(in) :: P(n+1)

	!fwd solve
	do i=1,n 
		x(i) = b(P(i))

		do k=1,i-1
			x(i) = x(i) - A(i, k) * x(k)
		end do 
	end do

	!bwd solve
	do i=n,1,-1
		do k=i+1,n 
			x(i) = x(i) - A(i, k) * x(k)
		end do 
		x(i) = x(i) / A(i, i)
	end do 

	end subroutine lu_fwdbwd_fp

	subroutine lu_solve_rp(n, Ain, b, x)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	use utility_m
	integer, value, intent(in) :: n 
	real(rp), intent(in) :: Ain(n, n), b(n) !Ain is read only
	real(rp), intent(out) :: x(n)
	real(rp) :: A(n, n)
	integer :: P(n+1), i, k

	A = Ain
	!Decomp
	call lu_decomp_rp(n, A, P)

	!call printmatrix_rp(A, n, n)
	!print *, "P: ", P(:)
	
	!fwd solve
	do i=1,n 
		x(i) = b(P(i))

		do k=1,i-1
			x(i) = x(i) - A(i, k) * x(k)
		end do 
	end do

	!bwd solve
	do i=n,1,-1
		do k=i+1,n 
			x(i) = x(i) - A(i, k) * x(k)
		end do 
		x(i) = x(i) / A(i, i)
	end do 

	end subroutine lu_solve_rp

	subroutine lu_decomp_rp(n, A, P)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	integer, value, intent(in) :: n
	real(rp), intent(inout) :: A(n, n) 
	integer, intent(out) :: P(n+1)
	integer :: i, j, k, imax 
	real(rp) :: maxA, absA, tmp(n), tol 

	tol = epsilon(tol)

	!initialize permutation
	do i=1,n 
		P(i) = i 
	end do

	!do inplace decomp
	do i=1,n 
		maxA = 0.0_rp 
		imax = i 

		do k=i,n 
			absA = abs(A(k, i))
			if (absA > maxA) then 
				maxA = absA
				imax = k 
			end if
		end do 

		if (maxA < tol) then 
			print *, "DEGENERATE MATRIX! OH NO!"
		end if 

		if (imax .ne. i) then 
			!pivoting P
			j = P(i)
			P(i) = P(imax)
			P(imax) = j 

			!pivoting rows of A 
			tmp(:) = A(i, :)
			A(i, :) = A(imax, :)
			a(imax, :) = tmp(:)

			P(n+1) = P(n+1) + 1 
		end if 

		do j=i+1,n 
			A(j, i) = A(j, i) / A(i, i)

			do k=i+1,n 
				A(j, k) = A(j, k) - A(j, i) * A(i, k)
			end do 
		end do  
	end do 

	end subroutine lu_decomp_rp

	subroutine lu_fwdbwd_rp(n, A, b, P, x)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	integer, value, intent(in) :: n 
	real(rp), intent(in) :: A(n, n), b(n) !A is a pre-decomposed matrix from lu_decomp_xx
	real(rp), intent(out) :: x(n)
	integer, intent(in) :: P(n+1)

	!fwd solve
	do i=1,n 
		x(i) = b(P(i))

		do k=1,i-1
			x(i) = x(i) - A(i, k) * x(k)
		end do 
	end do

	!bwd solve
	do i=n,1,-1
		do k=i+1,n 
			x(i) = x(i) - A(i, k) * x(k)
		end do 
		x(i) = x(i) / A(i, i)
	end do 
	
	end subroutine lu_fwdbwd_rp

end module mylu_m

module sdirk_m
contains	

	subroutine exact_solution(x, t, beta, uf)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: x(NX), t, beta
	real(fp), intent(out) :: uf(NX)
	real(fp) :: pi = 4.0*atan(1.0) !supposedly this is pi: https://stackoverflow.com/questions/2157920/why-define-pi-4atan1-d0
	integer :: i 

	do i=1,NX
		uf(i) = exp(-4.0_fp * beta * t * pi) * sin(2.0_fp * pi * x(i))
	end do

	end subroutine exact_solution

	subroutine init_u(u, x)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(fp), intent(inout) :: u(NX)
	real(fp), intent(in) :: x(NX)
	integer :: i
	real(fp) :: pi = 4.0*atan(1.0) !supposedly this is pi: https://stackoverflow.com/questions/2157920/why-define-pi-4atan1-d0
	!real(fp) :: pi = 3.14159
	u = 0.0
	do i=1,NX-1 
		u(i) = sin(2.0_fp * pi * x(i))
	end do 

	end subroutine init_u

	function fi_f(u, t, dx, beta) result(fn)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(fp), intent(in) :: u(NX), t, dx, beta 
	real(fp) :: fn(NX)
	integer :: i

	fn = 0.0

	do i=2,NX-1
		fn(i) = beta/(dx * dx) * (u(i-1) + u(i+1) - 2.0_fp * u(i)) 
	end do

	end function fi_f

	function fi_r(u, t, dx, beta) result(fn)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(rp), intent(in) :: u(NX), t, dx, beta 
	real(rp) :: fn(NX)
	integer :: i

	fn = 0.0

	do i=2,NX-1
		fn(i) = beta/(dx * dx) * (u(i-1) + u(i+1) - 2.0_rp * u(i)) 
	end do

	end function fi_r

	subroutine init_a(a, gamma)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(fp), value, intent(in) :: gamma
	real(fp), intent(inout) :: a(NX, NX)
	integer :: i 

	a = 0.0_rp
	do i=2,NX-1
		a(i, i+1) = gamma 
		a(i, i) = -2.0_rp * gamma 
		a(i, i-1) = gamma 
	end do
	a(1, 1) = 1.0_rp 
	a(NX, NX) = 1.0_rp 

	end subroutine init_a

	subroutine init_b(a, b, g)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	use utility_m
	implicit none
	real(rp), intent(in) :: a(NX, NX), g
	real(rp), intent(inout) :: b(NX, NX)

	call eye_rp(NX, b)
	b = b - g * a

	end subroutine init_b

	function update_step(u, t, dt, dx, beta, a, b, P) result(unp1)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	use utility_m
	use mylu_m
	implicit none
	real(fp), value, intent(in) :: t, dt, dx, beta
	real(fp), intent(in) :: u(NX), a(NX, NX)
	real(rp), intent(in) :: b(NX, NX)
	integer, intent(in) :: P(NX+1)
	real(fp) :: unp1(NX)
	real(fp) :: gamma, g, yk1(NX), yk2(NX)
	real(rp) :: y1(NX), y2(NX)
	integer :: p_corr=1, i

	!sqrt(3) = 1.7320508075688772_dp
	g = (1.7320508075688772_fp + 3.0_fp) / 6.0_fp

	!Stage 1
	!call lu_solve(NX, b, real(u, rp), y1)
	call lu_fwdbwd_rp(NX, b, real(u, rp), P, y1)

	!correction 1
	yk1 = real(y1, fp)
	do i=1,p_corr
		yk1 = u + matmul(a*g, yk1)
	end do 

	!stage 2
	!call lu_solve(NX, b, real((u + (1.0 - 2.0*g) * matmul(a, y1)), rp), y2)
	call lu_fwdbwd_rp(NX, b, real((u + (1.0 - 2.0*g) * matmul(a, y1)), rp), P, y2)

	!correction 2 
	yk2 = real(y2, fp)
	do i=1,p_corr
		yk2 = u + matmul((1.0 - 2.0*g)*a, yk1) + matmul(a*g, yk2)
	end do 

	!update step
	unp1 = u + matmul(a/2.0, yk1) + matmul(a/2.0, yk2)
	!if (DEBUG == 1) then
		!if (t .le. 0.000001) then !print only first one
		!	print *, "unp1: "
		!	call printmatrix_rp(unp1, NX, 1)
		!end if
	!end if

	end function update_step
	
	function sdirk_driver(nt, ts, xs, dx, init_dt, ti, tf, beta) result(u)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	use mylu_m
	implicit none 
	integer, value, intent(in) :: nt 
	real(fp), intent(in) :: ts(:), xs(NX), dx, init_dt, ti, tf, beta
	real(fp) :: t_f, dt, t, a(NX, NX), gamma, g 
	real(rp) :: b(NX, NX)
	real(fp) :: u(NX)
	integer :: i, P(NX+1)

	!initializations
	dt = init_dt
	gamma = real(beta * dt / (dx * dx), fp)
	g = (1.7320508075688772_fp + 3.0_fp) / 6.0_fp
	call init_u(u, xs)
	!initialize the matricies for LU. Only perform one LU decompose on b 
	!then use the fwdbwd on b each iteration.
	call init_a(a, gamma)
	call init_b(real(a, rp), b, real(g, rp))
	call lu_decomp_rp(NX, b, P)
	t_f = ti 
	!run time stepping loop
	do i=1,size(ts)
		t = ts(i)
		u = update_step(u, t, dt, dx, beta, a, b, P)
		t_f = t_f + dt 
	end do 

	if (DEBUG == 1) then
		print *, 'end time = ', t
		print *, 'final time = ', t_f
	end if

	end function sdirk_driver

end module sdirk_m 

program main
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE, qp=>real128
	use sdirk_m
	use utility_m 
	implicit none 
	integer :: nt
	real(qp) :: l2_err, tmp(NX), u_ref(NX), start, finish
	real(fp) :: beta, ti, tf, xi, xf, dx, dt
	real(fp) :: xs(NX), uf(NX)
	real(fp), allocatable :: ts(:)
	character(len=32) :: arg

	beta = 1.0_fp 
	ti = 0.0_fp 
	tf = 0.1_fp 
	xi = 0.0_fp 
	xf = 1.0_fp 

	call linspace(xi, xf, xs, .TRUE.) !spacial grid contains endpoint
	dx = xs(2) - xs(1)

	call getarg(1, arg)
	read(arg, *)nt 
	allocate(ts(nt))
	call linspace(ti, tf, ts, .FALSE.) !times does not contain endpoint
	dt = ts(2) - ts(1)
	if (DEBUG == 1) then 
		!print *, 'ts = ', ts(:)
		print *, 'dt = ', dt
	end if 

	call cpu_time(start)
	uf = sdirk_driver(nt, ts, xs, dx, dt, ti, tf, beta)
	call cpu_time(finish)
	print *, "method duration: ", finish-start

	if (DEBUG == 1) then
		!print *, "uf: ", uf(:)
	end if

	open(unit=2, file="../ref_sol/ref_sol.txt")
	read (2,*) u_ref(:) 
	close(2)

	tmp = real(uf, qp) - u_ref
	l2_err = sqrt(dx * dot_product(tmp, tmp))
	!l2_err = maxval(tmp)
	print *, "L2 Error with reference: ", l2_err

	deallocate(ts)

end program main