#!/bin/bash

#python plot_results.py $1 & PID1=$!
#python plot_results.py $2 & PID2=$!
#python plot_results.py $3 & PID3=$!
python plot_results_x86.py $1 & PID1=$!
python plot_results_x86.py $2 & PID2=$!
python plot_results_x86.py $3 & PID3=$!

wait $PID1
wait $PID2
wait $PID3
