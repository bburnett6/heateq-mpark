import json
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.cm as cm
import argparse as ap 
import os 

def plot_test_list(test, plot_en=False, save=False, save_type="png"):
	is_corr = False
	if ("novel" not in test) and (("1p" in test) or ("2p" in test) or ("3p" in test)):
		is_corr = True 
	if plot_en:
		with open(f'./{test}/energy_aarch64_{test}.json', 'r') as f:
			runs = json.load(f)
		if is_corr:
			with open(f'./{test[:-3]}/energy_aarch64_{test[:-3]}.json', 'r') as f:
				runs_nocorr = json.load(f)
	else:
		with open(f'./{test}/runs_x86_{test}.json', 'r') as f:
			runs = json.load(f)
		if is_corr:
			with open(f'./{test[:-3]}/runs_x86_{test[:-3]}.json', 'r') as f:
				runs_nocorr = json.load(f)

	if not os.path.exists(f'./images/{test}'):
		os.makedirs(f'./images/{test}')

	colors = cm.turbo(np.linspace(0, 1, len(runs))) #https://matplotlib.org/stable/tutorials/colors/colormaps.html
	for i, run in enumerate(runs):
		if is_corr and (run['full'] == run['redu']):
			plt.loglog(runs_nocorr[i]['dts'], runs_nocorr[i]['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(runs_nocorr[i]['dts'], runs_nocorr[i]['errs'], color=colors[i], s=(len(runs)-i)*20)
		else:
			plt.loglog(run['dts'], run['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(run['dts'], run['errs'], color=colors[i], s=(len(runs)-i)*20)

	#plt.title(f"Method: {test.upper()} for the\nVan der Pol T=[0,1]")
	plt.xlabel('dt')
	plt.ylabel('error')
	#plt.ylim(top=1e-3)
	plt.xticks(run['dts'])
	plt.grid()
	#plt.ylim(top=1e0)
	plt.legend()
	if not save:
		plt.show()
	else:
		plt.savefig(f'./images/{test}/err_v_dt.{save_type}', format=f'{save_type}', dpi=1000)
		plt.clf()

	for i, run in enumerate(runs):
		if is_corr and (run['full'] == run['redu']):
			plt.loglog(runs_nocorr[i]['dts'], runs_nocorr[i]['ts'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(runs_nocorr[i]['dts'], runs_nocorr[i]['ts'], color=colors[i], s=(len(runs)-i)*20)
		else:
			plt.loglog(run['dts'], run['ts'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(run['dts'], run['ts'], color=colors[i], s=(len(runs)-i)*20)

	#plt.title(f"Method: {test.upper()} for the\nVan der Pol T=[0,1]")
	plt.xlabel('dt')
	plt.ylabel('Runtime (s)')
	plt.legend()
	plt.grid()
	plt.yticks([10**(i) for i in range(-4, 5)])
	plt.xticks(run['dts'])
	plt.ylim(top=1e4)
	if not save:
		plt.show()
	else:
		plt.savefig(f'./images/{test}/ts_v_dt.{save_type}', format=f'{save_type}', dpi=1000)
		plt.clf()

	for i, run in enumerate(runs):
		if is_corr and (run['full'] == run['redu']):
			plt.loglog(runs_nocorr[i]['ts'], runs_nocorr[i]['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(runs_nocorr[i]['ts'], runs_nocorr[i]['errs'], color=colors[i], s=(len(runs)-i)*20)
		else:
			plt.loglog(run['ts'], run['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(run['ts'], run['errs'], color=colors[i], s=(len(runs)-i)*20)
	if is_corr:
		plt.title(f"Mixed:{test.upper()}\nNo-Mix:{test[:-3].upper()}")
	else:
		plt.title(f"{test.upper()}")
	plt.xlabel('Runtime (s)')
	plt.ylabel('Error')
	#plt.xlim(right=1e-1, left=1e-7)
	plt.legend()
	plt.grid()
	if not save:
		plt.show()
	else:
		plt.savefig(f'./images/{test}/ts_v_err.{save_type}', format=f'{save_type}', dpi=1000)
		plt.clf()

	if plot_en:
		for i, run in enumerate(runs):
			if is_corr and (run['full'] == run['redu']):
				plt.loglog(run['dts'], runs_nocorr[i]['core_pow'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
				plt.scatter(run['dts'], runs_nocorr[i]['core_pow'], color=colors[i], s=(len(runs)-i)*20)
			else:
				plt.loglog(run['dts'], run['core_pow'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
				plt.scatter(run['dts'], run['core_pow'], color=colors[i], s=(len(runs)-i)*20)

		#plt.title(f"Method: {test.upper()} for the\nVan der Pol T=[0,1]")
		plt.xlabel('dt')
		plt.ylabel('Power (W)')
		plt.xticks(run['dts'])
		#plt.ylim(top=1e0)
		#plt.ylim(bottom=14)
		plt.legend()
		plt.grid()
		if not save:
			plt.show()
		else:
			plt.savefig(f'./images/{test}/pow_v_r.{save_type}', format=f'{save_type}', dpi=1000)
			plt.clf()

		for i, run in enumerate(runs):
			if is_corr and (run['full'] == run['redu']):
				plt.loglog(runs_nocorr[i]['core_pow'], runs_nocorr[i]['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
				plt.scatter(runs_nocorr[i]['core_pow'], runs_nocorr[i]['errs'], color=colors[i], s=(len(runs)-i)*20)
			else:
				plt.loglog(run['core_pow'], run['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
				plt.scatter(run['core_pow'], run['errs'], color=colors[i], s=(len(runs)-i)*20)
		#plt.title(f"Method: {test.upper()} for the\nVan der Pol T=[0,1]")
		plt.xlabel('Power (W)')
		plt.ylabel('Error')
		#plt.xticks(dts)
		#plt.ylim(top=1e0)
		#plt.ylim(bottom=14)
		plt.legend()
		plt.grid()
		if not save:
			plt.show()
		else:
			plt.savefig(f'./images/{test}/pow_v_err.{save_type}', format=f'{save_type}', dpi=1000)
			plt.clf()

		for i, run in enumerate(runs):
			if is_corr and (run['full'] == run['redu']):
				plt.loglog(runs_nocorr[i]['ts'], runs_nocorr[i]['core_pow'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
				plt.scatter(runs_nocorr[i]['ts'], runs_nocorr[i]['core_pow'], color=colors[i], s=(len(runs)-i)*20)
			else:
				plt.loglog(run['ts'], run['core_pow'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
				plt.scatter(run['ts'], run['core_pow'], color=colors[i], s=(len(runs)-i)*20)
		#plt.title(f"Method: {test.upper()} for the\nVan der Pol T=[0,1]")
		plt.ylabel('Power (W)')
		plt.xlabel('Runtime (s)')
		#plt.xticks(dts)
		#plt.ylim(top=1e0)
		#plt.ylim(bottom=14)
		plt.legend()
		plt.grid()
		if not save:
			plt.show()
		else:
			plt.savefig(f'./images/{test}/pow_v_ts.{save_type}', format=f'{save_type}', dpi=1000)
			plt.clf()

def main():
	parser = ap.ArgumentParser(description='Plot the results of the tests!')
	parser.add_argument('test', metavar='T', type=str, help='Name of the test to plot (dir name)')
	parser.add_argument('--save', action='store_true', help='If set saves plots else display')
	args = parser.parse_args()
	plot_test_list(args.test, plot_en=True, save=args.save)

if __name__ == '__main__':
	main()