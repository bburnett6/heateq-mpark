
import numpy as np 
import subprocess as sp 
from math import ceil 

def round_nearest_hundred(x):
	return int(ceil(x / 100.0)) * 100

def run_ref_sol(nt):
	cmd = f"./rk4.x {nt} 1"
	out = sp.check_output(cmd, shell=True).decode("utf-8")
	for line in out.splitlines():
		if ("duration" in line):
			t = float(line.split()[-1])
		elif ("L2" in line):
			e = float(line.split()[-1])
	return t, e

def run_for_time_and_error(nt):
	cmd = f"./rk4.x {nt} 0"
	out = sp.check_output(cmd, shell=True).decode("utf-8")
	for line in out.splitlines():
		if ("duration" in line):
			t = float(line.split()[-1])
		elif ("L2" in line):
			e = float(line.split()[-1])
	return t, e 

if __name__ == '__main__':

	beta = 1.0
	ti = 0.0
	tf = 0.1
	xi = 0.0
	xf = 1.0

	nx = 20
	x = np.linspace(xi, xf, nx)
	dx = x[1] - x[0]
	print(f'Grid points: {nx}')

	min_r = 0.5 #r  = beta*dt/dx^2 => dt = r/beta*dx^2 => nt = beta/r/dx^2
	min_nt = round_nearest_hundred(beta / min_r / (dx*dx))

	ref_nt = 2**9 * min_nt

	ref_t, ref_e = run_ref_sol(ref_nt)

	nts = list(map(lambda p: p*100, range(6, 14) ))
	
	es = []
	ts = []
	dts = []
	for nt in nts:
		timesteps = np.linspace(ti, tf, nt, endpoint=False)
		dts.append(timesteps[1] - timesteps[0])
		t, e = run_for_time_and_error(nt)
		es.append(e)
		ts.append(t)

	orders = []
	for p in range(1, len(nts)):
		orders.append(np.log(es[p]/es[p-1])/np.log(dts[p]/dts[p-1]))

	print(f'orders: {orders}')
	print(f'average: {sum(orders)/len(orders)}')