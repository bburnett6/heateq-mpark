import subprocess as sp 
import os
import numpy as np 
#import matplotlib.pyplot as plt 
import json
import itertools
import time

"""
Compile using included makefile
d_redu - The reduced precision data type {real32, real64, real128}
d_full - The full precision data type {same ^}

no return

Note: I did try using subprocess but make wouldn't use the variables.
so I just used os.system
"""
def compile(d_full, d_redu, nx):
	cmd = f"make fp={d_full} rp={d_redu} debug=0 nx={nx}"
	#print(cmd)
	#print(cmd.split())
	#sp.run(['make', f"full_prec='{d_full}'", f"redu_prec='{d_redu}'"], shell=True, check=True)
	#sp.run(cmd.split(), shell=True, check=True)
	stat = os.system(cmd)
	if stat != 0:
		print(f"ERROR: exit status {stat}")

"""
Run the program using perf
nt - Program argument for nt, ie the number of timesteps

program outputs the time to run the method, so capture it and return.
perf stores data in a csv file. There is a convenient python wrapper
script to parse it provided by arm. So run that, and collect the data
to return as well.
"""
def run_prog(nt, test_name, n_runs=5):
	#dt computation. This will be the dt in the run
	ti = 0.0
	tf = 0.1
	timesteps = np.linspace(ti, tf, nt, endpoint=False)
	dt = timesteps[1] - timesteps[0]
	#on to the runs
	t = 0
	e = 0
	core_pow = 0
	core_en = 0
	l2_pow = 0
	mem_pow = 0
	for i in range(n_runs):
		#run perf and collect method output
		perf_file = 'data.perf'
		cmd = f'perf stat -x, -o {perf_file} -e duration_time,r11,r1e0,r3e0,r3e8 ./{test_name}.x {nt}'
		out = sp.run(cmd.split(), capture_output=True)
		#print(out.stdout.split())
		lines = out.stdout.split()
		t = t + float(lines[2])
		e = e + float(lines[7])
		#run the post processing script to collect energy data
		cmd = f'../postproc_perf_energy.py ./{perf_file}'
		out = sp.run(cmd.split(), capture_output=True)
		lines = out.stdout.split()
		core_pow = core_pow + float(lines[10])
		core_en = core_en + float(lines[14])
		l2_pow = l2_pow + float(lines[19])
		mem_pow = mem_pow + float(lines[24])
		#sleep a little for cleaner data
		time.sleep(5)

	return t/n_runs, e/n_runs, dt, core_pow/n_runs, core_en/n_runs, l2_pow/n_runs, mem_pow/n_runs

"""
Run the full experiment for one of the tests
test_name - the name of the test to run. Name must match the directory 
and teh name of the test.

Writes the results of the experiment inside the test directory to a 
json file
"""
def experiment(test_name):
	workdir = os.path.join('./', test_name)
	if not os.path.exists(workdir):
		print(f"experiment directory {test_name} doesn't exist. Returning...")
	os.chdir(workdir)

	d_types = ['real128', 'real64', 'real32', 'real16']
	nx = 20
	combos = itertools.combinations_with_replacement(d_types, 2)
	nts = list(map(lambda p: 10**p, range(2, 6)))
	
	runs = []
	for d_full, d_redu in combos:
		compile(d_full, d_redu, nx)
		time.sleep(3) #sleep a little so compile doesnt interfere with data collection
		run = {'redu': d_redu, 'full': d_full, 'errs': [], 'ts': [], 'dts': [], 'core_pow': [], 'core_en': [], 'l2_pow': [], 'mem_pow': []}
		for nt in nts:
			print(f"Running {test_name} for nt={nt}")
			t, e, dt, cp, ce, lp, mp = run_prog(nt, test_name)
			run['dts'].append(dt)
			run['errs'].append(e)
			run['ts'].append(t)
			run['core_pow'].append(cp)
			run['core_en'].append(ce)
			run['l2_pow'].append(lp)
			run['mem_pow'].append(mp)
		runs.append(run)

	with open(f'energy_aarch64_{test_name}.json', 'w') as f:
		json.dump(runs, f)
	os.chdir('..')

def main():
	#run the reference solution
	print("Running reference...")
	nx = 20
	os.chdir('./ref_sol')
	compile('real128', 'real128', nx)
	ref_nt = 10**9
	out = sp.run([f'./rk4.x', f'{ref_nt}', '1'], capture_output=True)
	os.chdir('./..')
	#run tests
	tests = ['impmid', 'impmid-1p', 'impmid-2p']
	for test in tests:
		experiment(test)

if __name__ == '__main__':
	main()